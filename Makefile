TOP ?= $(shell pwd)
TORCH := ${TOP}/.torch/install
SHELL := /bin/bash
PATH := bin:${TORCH}/bin:${PATH}

dev: clean buildc

# stack
build: bin
	stack build
buildc: bin
	stack build --file-watch
buildv: bin
	stack build --verbose
clean:; stack clean
clobber:; rm -rf .stack-work/*
init:; stack setup

LTS = $(shell egrep '^resolver:' stack.yaml | awk '{print $$2}')
bin:; ln -fs .stack-work/install/x86_64-linux/${LTS}/7.10.2/bin

char-rnn:
	cd lib/char-rnn && th train.lua -gpuid -1

install-torch: torch luarocks install-char-rnn

torch:
	if [ ! -d "${TOP}/.torch" ]; then \
		curl -s https://raw.githubusercontent.com/torch/ezinstall/master/install-deps | bash; \
		git clone https://github.com/torch/distro.git ${TOP}/.torch --recursive; \
		cd ${TOP}/.torch && ./install.sh; \
	fi

LUAROCKS := nngraph optim nn #cutorch cunn
luarocks:; source ${TORCH}/bin/torch-activate && for i in ${LUAROCKS}; do luarocks install $$i; done

install-char-rnn:
	git submodule add git@github.com:metaml/char-rnn lib/char-rnn

ld:
	sudo update-alternatives --install /usr/bin/ld ld /usr/bin/ld.gold 20
	sudo update-alternatives --install /usr/bin/ld ld /usr/bin/ld.bfd 10
	sudo update-alternatives --config ld

ld?:
	update-alternatives --query ld

.PHONY: dev build buildc buildv clean clobber init bin install-torch ld ld?
